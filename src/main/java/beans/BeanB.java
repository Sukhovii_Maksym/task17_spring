package beans;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import service.BeanValidator;

@Component
public class BeanB implements java.io.Serializable, BeanValidator, OurBean {
    @Value("${beanB.name:tequila}")
    private String name;
    @Value("${beanB.value:138.8}")
    private double value;


    public BeanB() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    private void init() {
        System.out.println("BeanB is initialized");
    }

    private void destroyed() {
        System.out.println("BeanB is destroyed");
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "name.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "value", "value.required");
        BeanB beanB = (BeanB) o;
        if (beanB.getValue() < 0) {
            errors.rejectValue("value", "can't be < 0");
        } else if (beanB.getValue() > 100) {
            errors.rejectValue("value", "can't be > 100");
        }
    }
}
