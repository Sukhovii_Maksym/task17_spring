package beans;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import service.BeanValidator;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

public class BeanE implements java.io.Serializable, BeanValidator {
    private String name;
    private double value;

    public BeanE() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "name.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "value", "value.required");
    }

    @PostConstruct
    public void postConstruct() {
        System.out.println("Bean E posConstruct");
    }

    @PreDestroy
    public void preDestroy() {
        System.out.println("Bean PreDestroyed");
    }
}
