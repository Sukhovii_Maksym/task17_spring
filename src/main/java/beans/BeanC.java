package beans;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import service.BeanValidator;

@Component
public class BeanC implements java.io.Serializable, BeanValidator {
    @Value("${beanC.name:horilka}")
    private String name;
    @Value("${beanC.value:40}")
    private double value;

    public BeanC() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    private void init() {
        System.out.println("BeanC is initialized");
    }

    private void destroyed() {
        System.out.println("BeanC is destroyed");
    }

    @Override
    public String toString() {
        return "BeanC{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "name.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "value", "value.required");
    }
}
