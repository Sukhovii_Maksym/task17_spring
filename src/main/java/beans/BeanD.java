package beans;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import service.BeanValidator;

public class BeanD implements java.io.Serializable, BeanValidator {
    @Value("${beanD.name:bear}")
    private String name;
    @Value("${beanD.value:5.5}")
    private double value;

    public BeanD() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    private void init() {
        System.out.println("BeanD is initialized");
    }

    private void destroyed() {
        System.out.println("BeanD is destroyed");
    }

    @Override
    public String toString() {
        return "BeanD{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "name.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "value", "value.required");
    }
}
