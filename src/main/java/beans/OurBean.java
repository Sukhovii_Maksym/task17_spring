package beans;

public interface OurBean {
    String getName();

    double getValue();
}
