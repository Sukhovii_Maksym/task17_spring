package beans;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import service.BeanValidator;

public class BeanA implements java.io.Serializable, BeanValidator, InitializingBean, DisposableBean, OurBean {
    private String name;
    private double value;

    public BeanA() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "name.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "value", "value.required");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("BeanA after Properties set");
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("BeanA Disposable destroyed");
    }
}
