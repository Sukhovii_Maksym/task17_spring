package service;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public interface BeanValidator extends Validator {

    @Override
    default boolean supports(Class<?> aClass) {
        return false;
    }

    @Override
    void validate(Object o, Errors errors);
}
