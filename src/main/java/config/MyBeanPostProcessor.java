package config;

import beans.OurBean;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

@Component
public class MyBeanPostProcessor implements BeanPostProcessor {


    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof OurBean)
            if (((OurBean) bean).getName() == null) {
                System.err.println(bean + "name is null");
            } else if (((OurBean) bean).getValue() < 0)
                System.err.println(bean + "value can't be less then 0");
            else if (((OurBean) bean).getValue() >= 100)
                System.err.println("Value can't be > 100");
            else System.out.println("Valid");
        return null;
    }
}
