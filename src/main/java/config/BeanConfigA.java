package config;

import beans.BeanA;
import beans.BeanB;
import beans.BeanC;
import beans.BeanD;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;


@Configuration
@ComponentScan
public class BeanConfigA {

    @Bean(initMethod = "init")
    public static MyFactoryBeanPostProcessor getMyFactoryBeanPostProcessor() {
        return new MyFactoryBeanPostProcessor();
    }

    @Bean("beanA_BC")
    @DependsOn(value = {"beanD", "beanB", "beanC"})
    public BeanA getBeanA_BC() {
        BeanB b = getBeanB();
        BeanC c = getBeanC();
        BeanA beanA = new BeanA();
        beanA.setName(b.getName() + "-" + c.getName());
        beanA.setValue((b.getValue() + c.getValue()) / 2);
        return beanA;
    }

    @Bean("beanA_BD")
    @DependsOn(value = {"beanD", "beanB", "beanC"})
    public BeanA getBeanA_BD() {
        BeanB b = getBeanB();
        BeanD d = getBeanD();
        BeanA beanA = new BeanA();
        beanA.setName(b.getName() + "-" + d.getName());
        beanA.setValue((b.getValue() + d.getValue()) / 2);
        return beanA;
    }

    @Bean("beanA_CD")
    @DependsOn(value = {"beanD", "beanB", "beanC"})
    public BeanA getBeanA_CD() {
        BeanD d = getBeanD();
        BeanC c = getBeanC();
        BeanA beanA = new BeanA();
        beanA.setName(c.getName() + "-" + d.getName());
        beanA.setValue((c.getValue() + d.getValue()) / 2);
        return beanA;
    }

    @Bean(name = "beanB", initMethod = "init", destroyMethod = "destroyed")
    public BeanB getBeanB() {
        return new BeanB();
    }

    @Bean(name = "beanC", initMethod = "init", destroyMethod = "destroyed")
    public BeanC getBeanC() {
        return new BeanC();
    }

    @Bean(name = "beanD", initMethod = "init", destroyMethod = "destroyed")
    public BeanD getBeanD() {
        return new BeanD();
    }
}
