package config;

import beans.BeanA;
import beans.BeanE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(BeanConfigA.class)
public class BeanConfigE {
    @Qualifier(value = "beanA_BC")
    @Autowired
    private BeanA a_BC;

    @Qualifier(value = "beanA_BD")
    @Autowired
    private BeanA a_BD;

    @Qualifier(value = "beanA_CD")
    @Autowired
    private BeanA a_CD;

    @Bean("beanE_BC")
    public BeanE getBeanE_BC() {
        BeanE beanE = new BeanE();
        beanE.setName(a_BC.getName() + " E-Edition");
        beanE.setValue(a_BC.getValue() + 5);
        return beanE;
    }

    @Bean("beanE_BD")
    public BeanE getBeanE_BD() {
        BeanE beanE = new BeanE();
        beanE.setName(a_BD.getName() + " E-Edition");
        beanE.setValue(a_BD.getValue() + 5);
        return beanE;
    }

    @Bean("beanE_CD")
    public BeanE getBeanE_CD() {
        BeanE beanE = new BeanE();
        beanE.setName(a_CD.getName() + " E-Edition");
        beanE.setValue(a_CD.getValue() + 5);
        return beanE;
    }


}
