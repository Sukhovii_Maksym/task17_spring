import beans.BeanA;
import beans.BeanE;
import config.BeanConfigE;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
    public static void main(String[] args) {

        ApplicationContext context = new AnnotationConfigApplicationContext(BeanConfigE.class);
        for (String beanDefinitionName : context.getBeanDefinitionNames()) {
            System.out.println(beanDefinitionName);
        }
        BeanA a_BC = (BeanA) context.getBean("beanA_BC");
        BeanA a_BD = (BeanA) context.getBean("beanA_BD");
        BeanA a_CD = (BeanA) context.getBean("beanA_CD");
        System.out.println(a_BC);
        System.out.println(a_BD);
        System.out.println(a_CD);

        BeanE e_BC = (BeanE) context.getBean("beanE_BC");
        BeanE e_BD = (BeanE) context.getBean("beanE_BD");
        BeanE e_CD = (BeanE) context.getBean("beanE_CD");
        System.out.println(e_BC);
        System.out.println(e_BD);
        System.out.println(e_CD);


    }
}

